# Step project

# [Ссылка на GitHub Pages](https://boikoyv.github.io/step-project/#!)

В проекте использовался нативный JavaScript. 

Для создания галереи в блоке "Gallery of best images" применялись библиотеки "Masonry" и "ImagesLoaded".

# Динамические эффекты на странице:

1. Переключение табов в блоке "Our Services";
2. Генерация, догрузка при клике на кнопку "Load more" и фильтрация карточек при выборе фильтра в блоке "Our Amazing Work";
3. Слайдер в блоке "What people say about theHam" с плавным переключением слайдов как стрелками, так и при выборе определенного аватара.
4. Преобразование галереи с помощью библиотек "Masonry" и "ImagesLoaded", догрузка фотографий при клике на кнопку "Load more" в блоке "Gallery of best images".
5. Имитация загрузки с сервера с помощью CSS анимации, скрытие кнопки догрузки, когда все файлы выгружены в блоках "Our Amazing Work" и "Gallery of best images";


